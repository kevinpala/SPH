#ifndef SPH_SYS_CPP
#define SPH_SYS_CPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>

#include "particle.h"
#include "SPH_System.h"

//____________________________________________________________

// Particle destructor definition

SPH_System::~SPH_System()
{

    for (int icell = 0; icell < ngrid; ++icell)
        delete[] grid_header;
    delete[] sph_particles;
}

//____________________________________________________________

// Particle constructor definition
SPH_System::SPH_System(double t, double h_c, int n, double L, double S)
{

    // parameter that will be used to create the particle vector
    double dx = 2. * L / n;

    // set system parameters
    tau = t;
    nparticles = n;
    total_quantity = S;
    h = h_c / sqrt(n/ 1000.);
    grid_spacing = 2. * h;
    grid_size = 6. * L;

    // creates the array that stores the particle pointers
    sph_particles = new particle[nparticles];

    ngrid = grid_size / grid_spacing;
    // Create array that will point to head particle in the cell (if not present)
    std::cout << ngrid << std::endl;
    std::cout << h << std::endl;

    grid_header = new particle *[(int)ngrid];
    double initial_shear_ee = 0;
    double initial_shear_xx = 0;
    double initial_energy = 101.368474404;
    double initial_b_number = 5.1;

    for (int ipart = 0; ipart < nparticles; ++ipart)
    {
        //initialize nparticles particles and then store their pointers at the array
        // here we are initializaing with energy = 1.0 and v=a=0.0 for all particles
        particle *p = new particle(-L + ipart * dx, 0.0, 0.0, initial_energy, initial_b_number, total_quantity / nparticles, total_quantity / (2. * L), tau,initial_shear_ee,initial_shear_xx);
        sph_particles[ipart] = *p;
        delete p;
    }
}

SPH_System::SPH_System(){};

//____________________________________________________________

// Spline Kernel

double SPH_System::SPH_kernel(double r1, double r2, double h)
{

    double result = 0.0;

    double dist = sqrt(pow(r1 - r2, 2.));
    double q = dist / h;
    double C = 1.0 / (6.0 * h);

    if (q >= 0. && q < 1.)
    {
        result = C * (pow((2.0 - q), 3.) - (4.0 * pow((1.0 - q), 3.)));
    }

    if (q >= 1. && q < 2.)
    {
        result = C * (pow((2.0 - q), 3.));
    }

    if (q >= 2.)
    {
        result = 0.0;
    }

    return result;
}

//Spline kernel spacial gradient

double SPH_System::spagrad_SPH_kernel(double r1, double r2, double h)
{
    double result = 0.0;

    double dist = sqrt(pow(r1 - r2, 2.));
    double q = dist / h;
    double C = 1.0 / (6.0 * h);

    if (q >= 0 && q < 1)
    {
        result = ((C / pow(h, 3.)) * 3.0 * (r1 - r2) * (3.0 * dist - 4.0 * h));
    }

    if (q >= 1 && q < 2)
    {
        result = (((-3.0 * C) / (pow(h, 3.) * dist)) * (r1 - r2) * (pow((dist - 2.0 * h), 2.)));
    }

    if (q >= 2)
    {
        result = 0.0;
    }

    return result;
}

//____________________________________________________________
// Set time
void SPH_System::set_time(double t) { tau = t; };

// return time
double SPH_System::get_time() { return tau; };

//____________________________________________________________

int SPH_System::get_particle_grid_index(particle *p) const
{

    // assing in wich cell the particle
    int grid_coordinate = std::floor((p->position / 2. + grid_size / 2.) / grid_spacing);
    p->particle_grid_index = grid_coordinate;

    // treats special cases in the grid borders
    if (grid_coordinate >= ngrid)
    {
        grid_coordinate = ngrid - 1;
    }
    if (grid_coordinate < 0)
    {
        grid_coordinate = 0;
    }
    return grid_coordinate;
}

//____________________________________________________________

void SPH_System::create_grid()
{

    //Clear particles linked list
    for (unsigned int ipart = 0; ipart < nparticles; ++ipart)
        sph_particles[ipart].next_particle = nullptr;

    // start as null
    for (int icell = 0; icell < ngrid; ++icell)
    {
        grid_header[icell] = nullptr;
    }

    // Place particles in the grid
    for (int ipart = 0; ipart < nparticles; ++ipart)
    { //loop over particles

        //gets in which cell the particle is
        int grid_idx = get_particle_grid_index(&(sph_particles[ipart]));
        //if this cell is empty the header will point to null, therefore
        // the header of the cell will be the first particle in this cell
        if (grid_header[grid_idx] == nullptr)
        {
            grid_header[grid_idx] = &(sph_particles[ipart]);
        }
        else
        {
            // if there is at least 1 particle in the in the cell, we
            //take this other particle and assign it as the next particle in the
            // cell, so the pointer next_particle of the header will point to
            // this next one
            particle *previous = grid_header[grid_idx];
            particle *next = previous->next_particle;

            // when next becomes null, we are at the end of the linked list
            // therefore, we loop over particles in the same cell
            // until we find the last inserted particle

            while (next != nullptr)
            {

                previous = next;
                next = previous->next_particle;
            }
            previous->next_particle = &(sph_particles[ipart]); //insert the particle
                                                               // icell at the tail
        }
    }
}

//____________________________________________________________

void SPH_System::set_initial_force_and_density_and_derivative(double t)
{
    for (int ipart = 0; ipart < nparticles; ++ipart)
    {
        //initial acceleration
        sph_particles[ipart].f_sph = 0.0;
        //initial density
        sph_particles[ipart].s_density = 0.0;
        sph_particles[ipart].d_s = 0.0;

        //initial spatial derivatives
        sph_particles[ipart].f1 = 0.0;
        sph_particles[ipart].f2 = 0.0;
        sph_particles[ipart].f3 = 0.0;
        sph_particles[ipart].f4 = 0.0;
    }
}

//____________________________________________________________

void SPH_System::calc_interction_forces(particle *p1, particle *p2, double t)
{
    // auxiliary variables

    double SPH_pressure = (p1->pressure / pow(p1->s_density, 2.)) + (p2->pressure / pow(p2->s_density, 2.));
    double Shear_pressure = (-p1->velocity*p2->shear_te_u+p2->shear_ee_u)/(p2->s_density*p2->s_density) + (-p2->velocity*p1->shear_te_u+p1-> shear_ee_u)/(pow(p1->s_density,2.));
    // assuming that s_nu is the same for every particle
    // for different values this expression must be changed
    double result = t * p1->s_nu * SPH_pressure * spagrad_SPH_kernel(p1->position, p2->position, h) +t*t*t*p1->s_nu*Shear_pressure* spagrad_SPH_kernel(p1->position, p2->position, h);

    p1->f_sph += result;
    p2->f_sph += -result;
}

//____________________________________________________________

void SPH_System::calc_interaction_density_and_derivative(particle *p1, particle *p2, double t)
{

    // assuming that s_nu is the same for every particle
    // for different values this expression must be changed
    double result1 = p1->s_nu * SPH_kernel(p1->position, p2->position, h);
    double result2 = p1->s_nu * (p1->velocity - p2->velocity) * spagrad_SPH_kernel(p1->position, p2->position, h);

    p1->s_density += result1;
    p2->s_density += result1;
    p1->d_s += result2;
    p2->d_s += result2;
}
//____________________________________________________________


void SPH_System::calc_interaction_spatial_derivative(particle *p1, particle *p2, double t)
{
    // assuming that s_nu is the same for every particle
    // for different values this expression must be changed

    double result1 = p2->s_nu * p2->velocity * p2->gamma *  spagrad_SPH_kernel(p1->position, p2->position, h)/p2->s_density;
    double result2 = p2->s_nu * p2->gamma *  spagrad_SPH_kernel(p1->position, p2->position, h)/p2->s_density;
    double result3 = p1->s_nu * p1->velocity * p1->gamma *  spagrad_SPH_kernel(p2->position, p1->position, h)/p1->s_density;
    double result4 = p1->s_nu * p1->gamma *  spagrad_SPH_kernel(p2->position, p1->position, h)/p1->s_density;

    double result5 = p2->s_nu*(p2->q_eta_u-p1->velocity*(t*t*p2->velocity*p2->q_eta_u))* spagrad_SPH_kernel(p1->position, p2->position, h)/p2->s_density;
    double result6 = p1->s_nu*(p1->q_eta_u-p2->velocity*(t*t*p1->velocity*p1->q_eta_u))* spagrad_SPH_kernel(p2->position, p1->position, h)/p1->s_density;
    double result7 = p2->s_nu * (p2->b_potential/p2->temperature) *  spagrad_SPH_kernel(p1->position, p2->position, h)/p2->s_density;
    double result8 = p1->s_nu * (p1->b_potential/p1->temperature) *  spagrad_SPH_kernel(p2->position, p1->position, h)/p1->s_density;
    p1->f1 += result1;
    p1->f2 += result2;
    p2->f1 += result3;
    p2->f2 += result4;


    p1->f3 += result5;
    p2->f3 += result6;
    p1->f4 += result7;
    p2->f4 += result8;

}
//____________________________________________________________

void SPH_System::att_particles_force_and_sderivatives(double t)
{
            // first shear terms
    for (int ipart =0; ipart < nparticles;ipart++){
        sph_particles[ipart].f_sph = 3.*t*t*sph_particles[ipart].shear_te_u/sph_particles[ipart].s_density + pow(t,3.)*sph_particles[ipart].d_shear_te_u/sph_particles[ipart].s_density - pow(t,3.)*sph_particles[ipart].shear_te_u*sph_particles[ipart].d_s/pow(sph_particles[ipart].s_density,2.);

    }

    for (int icell = 0; icell < ngrid; ++icell)
    { //Loops into all cells

        //get the header of the cell (first particle in the linked list)
        particle *p_trigger = grid_header[icell];

        while (p_trigger != 0x0)
        { //until we get at the end of the linked list
            // when we get null


            
            
            //Compute the force between "trigger" and all the other particles
            // in the same cell (until we find null = last particle in the cell)
            particle *p_target = p_trigger->next_particle;
            while (p_target != 0x0)
            {
                calc_interction_forces(p_trigger, p_target, t);
                calc_interaction_spatial_derivative(p_trigger,p_target,t);
                p_target = p_target->next_particle;
            }

            //now we need to compute the force between "trigger" and all the
            // the particles at he neighbor cells

            //   the left neighbor was already taken into account in the previous
            //  cell therefore we only need to compute the interactions between
            //   the particles in icell and their right neighbors

            p_target = grid_header[icell + 1];
            while (p_target != 0x0)
            {
                calc_interction_forces(p_trigger, p_target, t);
                calc_interaction_spatial_derivative(p_trigger,p_target,t);
                p_target = p_target->next_particle;
            }

            //Get trigger particle for next iteration
            p_trigger = p_trigger->next_particle;
        }
    }
}

//____________________________________________________________
void SPH_System::att_particles_density_and_derivative(double t)
{

    for (int icell = 0; icell < ngrid; ++icell)
    { //Loops into all cells

        //get the header of the cell (first particle in the linked list)
        particle *p_trigger_2 = grid_header[icell];

        while (p_trigger_2 != 0x0)
        { //until we get at the end of the linked list
            // when we get null

            // first compute the density caused by itself
            p_trigger_2->s_density += p_trigger_2->s_nu * SPH_kernel(p_trigger_2->position, p_trigger_2->position, h);

            //Compute the density due to "trigger" and all the other particles
            // in the same cell (until we find null = last particle in the cell)
            particle *p_target_2 = p_trigger_2->next_particle;
            while (p_target_2 != 0x0)
            {
                calc_interaction_density_and_derivative(p_trigger_2, p_target_2, t);
                p_target_2 = p_target_2->next_particle;
            }

            //now we need to compute the density due to "trigger" and all the
            // the particles at he neighbor cells

            //   the left neighbor was already taken into account in the previous
            //  cell therefore we only need to compute the density caused by
            //   the particles in icell and their right neighbors

            p_target_2 = grid_header[icell + 1];
            while (p_target_2 != 0x0)
            {
                calc_interaction_density_and_derivative(p_trigger_2, p_target_2, t);
                p_target_2 = p_target_2->next_particle;
            }

            //Get trigger particle for next iteration
            p_trigger_2 = p_trigger_2->next_particle;
        }
    }
}

//____________________________________________________________

void SPH_System::update_system(double t)
{
    create_grid();
    set_initial_force_and_density_and_derivative(t);
     for (int ipart = 0; ipart < nparticles; ++ipart)
    { 
        //assing thermodynamic variables using the EOS
        double nf = 2.5;
        double p0 = (16.+10.5*nf)*M_PI*M_PI/90.;
        sph_particles[ipart].temperature = pow(sph_particles[ipart].energy/(3.*p0),1./4.);
        sph_particles[ipart].pressure = sph_particles[ipart].energy/3.;
        sph_particles[ipart].entropy = (sph_particles[ipart].energy+sph_particles[ipart].pressure)/sph_particles[ipart].temperature;
        //calculate shear components using the restrictions
        sph_particles[ipart].shear_ee_u = sph_particles[ipart].shear_ee_l/(pow(t,4.));
        sph_particles[ipart].shear_tt_l =pow(t,4.)*sph_particles[ipart].velocity*sph_particles[ipart].velocity*sph_particles[ipart].shear_ee_u;
        sph_particles[ipart].shear_xx_l = sph_particles[ipart].shear_tt_l-sph_particles[ipart].shear_ee_l/(t*t);
        sph_particles[ipart].shear_tt_u = sph_particles[ipart].shear_tt_l;
        sph_particles[ipart].shear_te_u = sph_particles[ipart].velocity*sph_particles[ipart].shear_ee_l/(t*t);
        sph_particles[ipart].gamma = 1./sqrt(1.-pow(t*sph_particles[ipart].velocity,2.));
    }
    att_particles_density_and_derivative(t);
    att_particles_force_and_sderivatives(t);

    for (int ipart = 0; ipart < nparticles; ++ipart)
    {   
        

        sph_particles[ipart].calc_particle_atributes(t);
    }
}
#endif