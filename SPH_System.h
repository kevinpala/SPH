#ifndef SPH_SYS_H
#define SPH_SYS_H

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>

#include "particle.h"
class SPH_System
{

public:
    //Time
    double tau;

    // smoothing lenght
    double h;

    // total specific density
    double total_quantity;

    //Particle storage
    particle *sph_particles;
    int nparticles;

    //Grid parameters
    int ngrid;              // number of cells (1d)-- (0-1-2-3...-ngrid)
    double grid_spacing;    // Spacing of the grid
    double grid_size;       // Physical size of the grid
    particle **grid_header; //Array to store pointer to head particle in the                            //cells

    // SPH kernel and its spacial gradient
    double SPH_kernel(double r1, double r2, double h);
    double spagrad_SPH_kernel(double r1, double r2, double h);

    // grid functions

    void create_grid();                             // assign the grid headers and the pointers that point to                   // same cell particles
    int get_particle_grid_index(particle *p) const; // return in which grid cell                                               //  the particle is

    //functions to update the particles parameters that dependes on
    // particle-particle interaction

    // external forces and density of the particle itself(does not depend on other
    // particles)
    void set_initial_force_and_density_and_derivative(double t);

    //Use SPH to calculate the force between particles
    void calc_interction_forces(particle *p1, particle *p2, double t);

    //Use SPH to calculate the density contribution from another particle
    void calc_interaction_density_and_derivative(particle *p1, particle *p2, double t);

    //Use SPH to calculate all the spatial derivatives contribution from another particle
    void calc_interaction_spatial_derivative(particle *p1,particle *p2,double t);
    
    //Use grid to calculate,force,density and spatial derivatives for each particle in the system
    void att_particles_force_and_sderivatives(double t);
    void att_particles_density_and_derivative(double t);
    void att_partial_derivatives(double t);

    //Calculate the spatial derivatives using the SPH method, and then atribute its value to each particle
    void update_spatial_derivatives(double t);

    //Updates all parameters in the system (that are not calculated using RK4)
    void update_system(double t);


    void att_thermodynamical_quantities(double t);
    std::vector<double> thermodynamic_variables;

    // time control functions

    double get_time(); //return time

    void set_time(double t); // set time

    //constructor
    SPH_System();
    SPH_System(double t, double h_c, int n, double L, double S);
    //destructor
    ~SPH_System();
};
#endif