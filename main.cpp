

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>

#include "SPH_System.h"

//____________________________________________________________

//____________________________________________________________

int main(int argc, char *argv[])
{

    // initial time, max time and time step
    double T_max = 5.0;
    double t_init = 1.0;
    double dt = 0.01;
    double t = t_init;

    // Runge-kutta vectors
    double RK_K1[360];
    double RK_K2[360];
    double RK_K3[360];
    double RK_K4[360];
    double RK_L1[360];
    double RK_L2[360];
    double RK_L3[360];
    double RK_L4[360];
    double RK_M1[360];
    double RK_M2[360];
    double RK_M3[360];
    double RK_M4[360];
    double RK_N1[360];
    double RK_N2[360];
    double RK_N3[360];                                          
    double RK_N4[360];
    double RK_V1[360];
    double RK_V2[360];
    double RK_V3[360];
    double RK_V4[360];
    double RK_C1[360];
    double RK_C2[360];
    double RK_C3[360];
    double RK_C4[360];
    double RK_Q1[360];
    double RK_Q2[360];
    double RK_Q3[360];
    double RK_Q4[360];

    //initialize the system with the initial conditions
    SPH_System sys(t_init, 0.08, 360, 2.0, 4.0);
    sys.update_system(t_init);
    std::ofstream myfile;
    std::string out_string;
    
    out_string = "central_shear_diff_3_anal.csv";
    myfile.open(out_string.c_str());
            myfile << "eta"
               << ","
               << "v"
               << ","
               << "s"
               << ","
               << "f_sph"
               << ","
               << "aceleration"
               << ","
               << "energy"
               << ","

               << "N"
               << ","
               << "tau"
               << ","
               << "Pressure"
               << ","
               << "T" 
               << ","
               << "shear_xx_l" 
               << ","
               << "shear_ee_l" 
               << ","
               << "q_eta_u" 
               << std::endl;

    // main loop
    while (t <= T_max)
    {

        //function to save a csv with the values

        std::ofstream myfile2;
        std::string out_string2;
        std::stringstream ss;
        std::stringstream sh;
        ss << t;
        std::cout << t << std::endl;
        
        out_string2 = "t-" + ss.str() + "_shear_diff.csv";

        myfile2.open(out_string2.c_str());
            myfile2 << "eta"
               << ","
               << "v"
               << ","
               << "s"
               << ","
               << "f_sph"
               << ","
               << "aceleration"
               << ","
               << "energy"
               << ","

               << "N"
               << ","
               << "tau"
               << ","
               << "Pressure"
               << ","
               << "T" 
               << ","
               << "mu" 
               << ","
               << "shear_ee_l" 
               << ","
               << "q_eta_u" 
               << std::endl;
        for (int i = 0; i < sys.nparticles; i++)
        {
        myfile2 << sys.sph_particles[i].position << "," << sys.sph_particles[i].velocity << "," << sys.sph_particles[i].s_density << "," << sys.sph_particles[i].f_sph << "," << sys.sph_particles[i].acceleration << "," << sys.sph_particles[i].energy << "," << sys.sph_particles[i].b_number <<"," << t <<"," << sys.sph_particles[i].pressure<<"," << sys.sph_particles[i].temperature<<","<< sys.sph_particles[i].b_potential<< "," << sys.sph_particles[i].shear_ee_l<<"," << sys.sph_particles[i].q_eta_u<<std::endl;
            }

        myfile2.close();
        std::cout<< t<<std::endl;

        for (int i = 0; i < sys.nparticles; i++)
        {
            double x = sqrt(pow(sys.sph_particles[i].position,2.));
            //std::cout<< x <<std::endl;
            if(x <= 0.0001){
                //std::cout<< x <<std::endl;
                myfile << sys.sph_particles[i].position << "," << sys.sph_particles[i].velocity << "," << sys.sph_particles[i].s_density << "," << sys.sph_particles[i].f_sph << "," << sys.sph_particles[i].acceleration << "," << sys.sph_particles[i].energy << "," << sys.sph_particles[i].b_number <<"," << t <<"," << sys.sph_particles[i].pressure<<"," << sys.sph_particles[i].temperature<<","<< sys.sph_particles[i].d_shear_ee_l<< "," << sys.sph_particles[i].shear_ee_l<<"," << sys.sph_particles[i].q_eta_u<<std::endl;
            }
            else{}
            
        }

        
     // RK4 integration
        for (int ipart = 0; ipart < sys.nparticles; ipart++)
        {
            sys.sph_particles[ipart].set_original_values(t); // store the variables values at the begning of the time step

            RK_K1[ipart] = dt * sys.sph_particles[ipart].velocity;
            RK_L1[ipart] = dt * sys.sph_particles[ipart].acceleration;
            RK_M1[ipart] = dt * sys.sph_particles[ipart].d_energy;
            RK_N1[ipart] = dt * sys.sph_particles[ipart].d_b_number;
            RK_C1[ipart] = dt * sys.sph_particles[ipart].d_shear_ee_l;
            RK_Q1[ipart] = dt * sys.sph_particles[ipart].d_q_eta_u;

            sys.sph_particles[ipart].position = sys.sph_particles[ipart].original_position + RK_K1[ipart] / 2.;
            sys.sph_particles[ipart].velocity = sys.sph_particles[ipart].original_velocity + RK_L1[ipart] / 2.;
            sys.sph_particles[ipart].energy = sys.sph_particles[ipart].original_energy + RK_M1[ipart] / 2.;
            sys.sph_particles[ipart].b_number = sys.sph_particles[ipart].original_b_number + RK_N1[ipart] / 2.;
            sys.sph_particles[ipart].shear_ee_l = sys.sph_particles[ipart].original_shear_ee_l + RK_C1[ipart]/2.;
            sys.sph_particles[ipart].q_eta_u = sys.sph_particles[ipart].original_q_eta_u + RK_Q1[ipart]/2.;
        }

        sys.update_system(t + dt / 2.);

        for (int ipart = 0; ipart < sys.nparticles; ipart++)
        {

            RK_K2[ipart] = dt * sys.sph_particles[ipart].velocity;
            RK_L2[ipart] = dt * sys.sph_particles[ipart].acceleration;
            RK_M2[ipart] = dt * sys.sph_particles[ipart].d_energy;
            RK_N2[ipart] = dt * sys.sph_particles[ipart].d_b_number;
            RK_C2[ipart] = dt * sys.sph_particles[ipart].d_shear_ee_l;
            RK_Q2[ipart] = dt * sys.sph_particles[ipart].d_q_eta_u;


            sys.sph_particles[ipart].position = sys.sph_particles[ipart].original_position + RK_K2[ipart] / 2.;
            sys.sph_particles[ipart].velocity = sys.sph_particles[ipart].original_velocity + RK_L2[ipart] / 2.;
            sys.sph_particles[ipart].energy = sys.sph_particles[ipart].original_energy + RK_M2[ipart] / 2.;
            sys.sph_particles[ipart].b_number = sys.sph_particles[ipart].original_b_number + RK_N2[ipart] / 2.;
            sys.sph_particles[ipart].shear_ee_l = sys.sph_particles[ipart].original_shear_ee_l + RK_C2[ipart]/2.;
            sys.sph_particles[ipart].q_eta_u = sys.sph_particles[ipart].original_q_eta_u + RK_Q2[ipart]/2.;
        }

        sys.update_system(t + dt / 2.);

        for (int ipart = 0; ipart < sys.nparticles; ipart++)
        {

            RK_K3[ipart] = dt * sys.sph_particles[ipart].velocity;
            RK_L3[ipart] = dt * sys.sph_particles[ipart].acceleration;
            RK_M3[ipart] = dt * sys.sph_particles[ipart].d_energy;
            RK_N3[ipart] = dt * sys.sph_particles[ipart].d_b_number;
            RK_C3[ipart] = dt * sys.sph_particles[ipart].d_shear_ee_l;
            RK_Q3[ipart] = dt * sys.sph_particles[ipart].d_q_eta_u;


            sys.sph_particles[ipart].position = sys.sph_particles[ipart].original_position + RK_K3[ipart];
            sys.sph_particles[ipart].velocity = sys.sph_particles[ipart].original_velocity + RK_L3[ipart];
            sys.sph_particles[ipart].energy = sys.sph_particles[ipart].original_energy + RK_M3[ipart];
            sys.sph_particles[ipart].b_number = sys.sph_particles[ipart].original_b_number + RK_N3[ipart];
            sys.sph_particles[ipart].shear_ee_l = sys.sph_particles[ipart].original_shear_ee_l + RK_C3[ipart];
            sys.sph_particles[ipart].q_eta_u = sys.sph_particles[ipart].original_q_eta_u + RK_Q3[ipart];
        }

        sys.update_system(t + dt);

        for (int ipart = 0; ipart < sys.nparticles; ipart++)
        {
            RK_K4[ipart] = dt * sys.sph_particles[ipart].velocity;
            RK_L4[ipart] = dt * sys.sph_particles[ipart].acceleration;
            RK_M4[ipart] = dt * sys.sph_particles[ipart].d_energy;
            RK_N4[ipart] = dt * sys.sph_particles[ipart].d_b_number;
            RK_C4[ipart] = dt * sys.sph_particles[ipart].d_shear_ee_l;
            RK_Q4[ipart] = dt * sys.sph_particles[ipart].d_q_eta_u;

            sys.sph_particles[ipart].position = sys.sph_particles[ipart].original_position + (RK_K1[ipart] + 2. * RK_K2[ipart] + 2. * RK_K3[ipart] + RK_K4[ipart]) / 6.;
            sys.sph_particles[ipart].velocity = sys.sph_particles[ipart].original_velocity + (RK_L1[ipart] + 2. * RK_L2[ipart] + 2. * RK_L3[ipart] + RK_L4[ipart]) / 6.;
            sys.sph_particles[ipart].energy = sys.sph_particles[ipart].original_energy + (RK_M1[ipart] + 2. * RK_M2[ipart] + 2. * RK_M3[ipart] + RK_M4[ipart]) / 6.;
            sys.sph_particles[ipart].b_number = sys.sph_particles[ipart].original_b_number + (RK_N1[ipart] + 2. * RK_N2[ipart] + 2. * RK_N3[ipart] + RK_N4[ipart]) / 6.;
            sys.sph_particles[ipart].shear_ee_l = sys.sph_particles[ipart].original_shear_ee_l + (RK_C1[ipart] + 2. * RK_C2[ipart] + 2. * RK_C3[ipart] + RK_C4[ipart]) / 6.;
            sys.sph_particles[ipart].q_eta_u = sys.sph_particles[ipart].original_q_eta_u + (RK_Q1[ipart] + 2. * RK_Q2[ipart] + 2. * RK_Q3[ipart] + RK_Q4[ipart]) / 6.;
        }

        sys.update_system(t + dt);

        t += dt;
    }

    std::cout << "Done!" << std::endl;
}
