#ifndef PARTICLE_CPP
#define PARTICLE_CPP

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>

#include "particle.h"


// Particle constructor definition
particle::particle(double r, double v, double a, double e, double n, double nu, double s, double t,double ee,double xx)
{
    energy = e;
    b_number = n;
    pressure = e/3.;
    dp_de = 1./3.;
    double nf = 2.5;
    double p0 = (16.+10.5*nf)*M_PI*M_PI/90.;
    temperature = pow(e/(3.*p0),1./4.);
    b_potential = 0.0;
    entropy = (energy+pressure-b_potential*b_number)/temperature;
    c_e = pow(15./(128.*M_PI*M_PI),1./3.);
    position = r;
    velocity = v;
    acceleration = 0.0;
    
    s_nu = nu;
    s_density = s;
    d_s = 0.0;
    gamma = sqrt(1. / (1. - (pow(t * velocity, 2.))));
    d_gamma = pow(velocity, 2.) * pow(gamma, 3.) * t + t * t * pow(gamma, 3.) * velocity * acceleration;
    theta = -gamma * d_s / s_density + d_gamma + (gamma / t);
    
    entropy = pow(energy/(3.*c_e),3./4.);
    c_shear = 0.08*entropy;
    tr_shear = 5.*c_shear/(energy+pressure);
    sigma_ee = -t*t*f1 - t*gamma - gamma*gamma*t*t*velocity*(t*t*acceleration*gamma + 2.*t*gamma*velocity + t*t*velocity*d_gamma) + theta*(t*t + t*t*t*t*velocity*velocity*gamma*gamma)/3.;
    d_b_number = 0.0;
    d_energy = 0.0;
    shear_ee_l = -4.*c_shear*t/3.;
    shear_ee_u = shear_ee_l/(pow(t,4.));
    shear_tt_l =pow(t,4.)*velocity*velocity*shear_ee_u;
    shear_xx_l = shear_tt_l-shear_ee_l/(t*t);
    shear_tt_u = shear_tt_l;
    shear_te_u = velocity*shear_ee_l/(t*t);
    d_shear_ee_l = 0.0;
    d_shear_xx_l =0.0;
    d_shear_te_u = 0.0;
    f1 = 0.0;
    f2 =0.0;
    f3 =0.0;
    f4 = 0.0;
    q_eta_u = 1.0;
    q_tau_l = t*t*velocity*q_eta_u;
    q_tau_u = q_tau_l;
    q_eta_l = -t*t*q_eta_u;
    c_shear = 0.08*entropy;
    d_q_eta_u = 0.0;
    tr_diffusion = 0.08/temperature;
    tr_shear = 5.*c_shear/(energy+pressure);
    calc_particle_atributes(t);
};

particle::particle(){};

//____________________________________________________________
// calc_particle_acceleration definition 
double particle::calc_particle_acceleration(double t)
{

    double w = energy + pressure;
    double A = (1. + dp_de) ;
    double alpha = A*w;
    
    double H = (shear_tt_u*(pow(velocity,2.)*t*pow(gamma,3.) - velocity*f2) + shear_te_u*(-pow(t*velocity*gamma,3.) + t*t*velocity*f1 + f2) + shear_ee_u*(-t*t*f1 - t*gamma))/gamma;
    double T = -s_density * f_sph + gamma * gamma * velocity * pow(t, 3.) * w * d_s / s_density - 3. * pow(gamma * t, 2.) * velocity * w - 2. * pow(velocity, 3.) * pow(gamma , 4.)*pow(t,4.) * w + w * A* pow(gamma * t, 2.) * velocity + pow(gamma * t, 4.) * w * A * pow(velocity, 3.) - gamma * gamma * velocity * pow(t, 3.) * A*w * d_s / s_density-A*gamma*gamma*velocity*pow(t,3.)*H;
    double B = t*t*t*gamma*gamma*w + 2.*pow(t,5.)*pow(gamma,4.)*pow(velocity,2.)*w -A*w*pow(velocity,2.)*pow(gamma,4.)*pow(t,5.)+shear_tt_u*A*pow(gamma,4.)*pow(t,5.)*pow(velocity,2.)-shear_te_u*A*pow(t,7.)*pow(velocity,3.)*pow(gamma,4.)-shear_te_u*A*pow(t,5.)*pow(gamma,2.)*velocity;

    double a = T / B;
    return a;
}

//____________________________________________________________
// calculate dN/dtau
double particle::calc_dn_dt(double t)
{

    double dn = -b_number*theta/gamma - q_tau_l/(gamma*t) - d_q_tau_u - f3/gamma ;
    return dn;
}


//____________________________________________________________w
void particle::calc_shear_derivative(double t)
{

    double result1;
    double result2;
    double result3;

    sigma_ee = -t*t*f1 - t*gamma - gamma*gamma*t*t*velocity*(t*t*acceleration*gamma + 2.*t*gamma*velocity + t*t*velocity*d_gamma) + theta*(t*t + t*t*t*t*velocity*velocity*gamma*gamma)/3.;
    result1 = -2.*velocity*pow(t,3.)*shear_te_u + 2.*shear_ee_l/t - 2.*velocity*gamma*shear_ee_l*(2.*t*gamma*velocity + t*t*gamma*acceleration + pow(t,2.)*velocity*d_gamma) 
    + 2.*gamma*pow(t,4.)*velocity*shear_te_u*(d_gamma + t*gamma*pow(velocity,2.)) - (4.*theta/3.+1./tr_shear)*shear_ee_l/gamma + 2.*c_shear*sigma_ee/(gamma*tr_shear);
    d_shear_ee_l = result1;

    result2 = -(1./tr_shear + 4.*theta/3.)*shear_xx_l/gamma + 2.*c_shear*theta/(3.*gamma*tr_shear);
    d_shear_xx_l = result2;
    result3 = acceleration*shear_ee_l/(t*t) - 2.*velocity*shear_ee_l/(pow(t,3.)) + velocity*d_shear_ee_l/(t*t);
    d_shear_te_u = result3;

}
//____________________________________________________________
void particle::calc_diffusion_derivative(double t)
{
    double result2;
    
    result2 = gamma*q_eta_u/t + gamma*velocity*q_tau_u/t + gamma*velocity*(q_tau_l*gamma*d_gamma+q_tau_l*pow(gamma,2.)*pow(velocity,2.)*t
    +2.*q_eta_l*gamma*gamma*velocity/t +q_eta_l*gamma*velocity*d_gamma+q_eta_l*gamma*gamma*acceleration);

    d_q_eta_u = -result2/gamma;

    d_q_tau_u = 2.*t*velocity*q_eta_u + t*t*q_eta_u*acceleration + t*t*velocity*d_q_eta_u; //we also calculate the other component here
                                                                                           //using the restrictions since it will appear 
                                                                                           //on dn equation
}
//____________________________________________________________

// calc_particle_atributes definition
void particle::calc_particle_atributes(double t)
{
    // calculate all thermodynamics variables using energy and barionic number
    pressure = energy/3.;
    dp_de = 1./3.;
    double nf = 2.5;
    double p0 = (16.+10.5*nf)*M_PI*M_PI/90.;
    temperature = pow(energy/(3.*p0),1./4.);
    entropy = (energy+pressure)/temperature;
    //shear variables
    tr_diffusion = 0.08/temperature;
    c_shear = 0.08*entropy;
    tr_shear = 5.*c_shear/(energy+pressure);
    //kinematics variables
    gamma = 1. / sqrt(1. - (pow(t * velocity, 2.)));
    acceleration = calc_particle_acceleration(t);
    d_gamma = pow(velocity, 2.) * pow(gamma, 3.) * t + t * t * pow(gamma, 3.) * velocity * acceleration;
    theta = -gamma * d_s / s_density + d_gamma + (gamma / t);
    //calculate other components using the restrictions
    shear_ee_u = shear_ee_l/(pow(t,4.));
    shear_tt_l =pow(t,4.)*velocity*velocity*shear_ee_u;
    shear_xx_l = (shear_tt_l-shear_ee_l/(t*t))/2.;
    shear_tt_u = shear_tt_l;
    shear_te_u = velocity*shear_ee_l/(t*t);
    q_tau_l = t*t*velocity*q_eta_u;
    q_tau_u = q_tau_l;
    q_eta_l = -t*t*q_eta_u;
    //calculate viscosity tensor derivative
    calc_shear_derivative(t);
    //calculate diffusion vector derivative
    calc_diffusion_derivative(t);
    // derivatives using energy-momentum tensor expressions
    // the terms that depend on the viscosity tensor on the energy derivative equation
    double shear_terms = shear_tt_u*(d_gamma - velocity*f2) + shear_te_u*(-t*t*velocity*d_gamma - t*t*gamma*acceleration + t*t*velocity*f1 + f2) + shear_ee_u*(-t*t*f1 - t*gamma);
    // the terms that depend on the diffusion vector on the baryon density derivative equation
    double diffusion_terms = -q_tau_u/(gamma*t) - d_q_tau_u - f3/gamma;
    d_energy = -(energy + pressure) * theta / gamma + shear_terms / gamma;
    d_b_number = -b_number*theta/gamma +diffusion_terms;
    
};
//____________________________________________________________


void particle::set_original_values(double t)
{

    original_energy = energy;
    original_position = position;
    original_velocity = velocity;
    original_b_number = b_number;
    original_shear_ee_l = shear_ee_l;
    original_shear_xx_l = shear_xx_l;
    original_q_eta_u = q_eta_u;
};

#endif