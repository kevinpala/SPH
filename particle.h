
#ifndef PARTICLE_H
#define PARTICLE_H

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>


class particle
{
public:
    particle *next_particle; //A pointer to the next particle in the same cell as the current one

    // kinematics variables
    double position;
    double velocity;
    double acceleration;
    double theta;   // fluid expansion rate \nabla_\mu u^\mu
    double gamma;   // lorentz factor
    double d_gamma; // lorentz facotr derivative

    // thermodynamics variables
    double energy;
    double d_energy; // energy time derivative
    double pressure;
    double temperature;
    double entropy;
    double s_density;   // Specific density
    double s_nu;        // Total specific density / # of particles
    double d_s;         // Time derivative of the specific density
    double b_number;    // Baryonic number
    double d_b_number;  // Baryonic number time derivative
    double b_potential; // Baryonic potential
    double dp_de;       //derivative of the pressure with respect to the energy



    double c_e;
    double c_T;
    double sigma_ee;



    //dissipative variables
    double c_shear;      //shear coefficient
    double c_bulk;       //bulk coefficient
    double c_diff;       // shear coefficient
    double tr_bulk;      // bulk relaxation time
    double tr_shear;     // shear relaxation time
    double tr_diffusion; // diffusion relaxation time

    // bulk and derivative
    double bulk;  // bulk pressure (Pi)
    double dbulk; // bulk pressure time derivative

    //diffusion current components
    double q_eta_l;   // eta component (lower index)
    double q_tau_l;   // eta component (lower index)
    double q_tau_u;   // tau component (upper index)
    double q_eta_u;  //  eta component (upper index)
    
    double d_q_eta_u; // eta component time derivative
    double d_q_tau_u; // tau component derivative

    double d_q_eta_l; // eta component time derivative
    double d_q_tau_l;

    //

    // shear tensor components (lower index)
    double shear_ee_l;   //eta eta component
    double shear_tt_l;   //tau tau component
    double shear_te_l;   //tau eta component
    double shear_xx_l;   // x x component
    double shear_yy_l;   // y y component
    double d_shear_ee_l; //eta eta component time derivative
    double d_shear_tt_l; //tau tau component time derivative
    double d_shear_te_u; //tau eta component time derivative (upper index)
    double d_shear_te_l; //tau eta component time derivative
    double d_shear_xx_l; // x x component time derivative
    double d_shear_yy_l; // y y component time derivative

    // shear tensor components (upper index)
    double shear_ee_u; //eta eta component
    double shear_tt_u; //tau tau component
    double shear_te_u; //tau eta component
    double shear_xx_u; // x x component
    double shear_yy_u; // y y component

    // auxiliary variables

    // these variables store spatial partial derivatives that are calculated using sph
    double f1; //gamma*velocity spatial derivative
    double f2; //gamma spatial derivative
    double f3; // diffusion vector spatial derivative
    double f4; // mu/T spatial derivative
    double f5;

    double f_sph; // will store the force due to particle interactions

    //these variables store the values that will be evolved with RK4
    double original_energy;
    double original_position;
    double original_velocity;
    double original_b_number;
    double original_shear_ee_l;
    double original_shear_xx_l;
    double original_q_eta_u;

    // grid index of the particle
    int particle_grid_index;

    //Constructors
    particle();
    particle(double r, double v, double a, double e, double n, double nu, double s, double t,double ee,double xx);

    //sph_force, specific density and the spacial derivatives will be calculated
    // in the System , since they depend on other particles

    // calculate all particles variables, uses the calc_particle_acceleration
    //function
    void calc_particle_atributes(double t);
    double calc_particle_acceleration(double t);
    double calc_dn_dt(double t);

    //Calculate the shear components derivatives
    void calc_shear_derivative(double t); 

    //Calculate diffusion components derivatives:
    void calc_diffusion_derivative(double t);

    // set values to the original variables
    void set_original_values(double t);
};

#endif